import { defineConfig }    from 'vite';
import { createVuePlugin } from 'vite-plugin-vue2';
import Checker             from 'vite-plugin-checker';
import * as path           from 'path';

export default defineConfig({
  plugins: [[createVuePlugin(), Checker({ vueTsc: true })]],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
  },
  build: {
    rollupOptions: {
      output: {
        manualChunks(id: string) {
          if (id.includes('sample')) {
            return `sample`;
          }
        }
      }
    }
  }
});
