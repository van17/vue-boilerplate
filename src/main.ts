import Vue from 'vue';
import App from '@/app.vue';
import router from '@/routes';
import { store } from '@/stores';

import '@/assets/scss/main.scss';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
  router,
  store,
}).$mount('#app');
 