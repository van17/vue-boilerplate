import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [];
const modules = import.meta.globEager('@/**/*.router.ts');

for (const path in modules) {
  modules[path].default.forEach((routerItem: RouteConfig) => {
    routes.push(routerItem);
  });
}

const router = new VueRouter({
  mode: 'history',
  routes: routes,
});

export default router;
