const SharedRouter = [
  {
    path    : '/',
    redirect: '/sample/list',
  },
];

export default SharedRouter;