import axios from 'axios';

export const baseUrl = () => {
  return axios.create({
    baseURL: import.meta.env.VITE_APP_API
  });
};