import 'reflect-metadata';
import { Expose } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';

export namespace Sample {
  export namespace Request {
    export class Add {
      @Expose()
      @IsString() @IsNotEmpty()
        userId!: string;

      @Expose()
      @IsString()
        title!: string;

      @Expose()
      @IsString()
        body!: string;
    }
  }

  export namespace Response {
    export class FindAll {
      @Expose()
        id!: number;

      @Expose()
        userId!: number;

      @Expose()
        body!: string;

      @Expose()
        title!: string;
    }

    export class FindOne {
      @Expose()
        id!: number;

      @Expose()
        userId!: number;

      @Expose()
        body!: string;

      @Expose()
        title!: string;
    }
  }
}
