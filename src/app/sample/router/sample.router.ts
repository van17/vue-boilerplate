const SampleRouter = [
  {
    path: '/sample',
    redirect : '/sample/list',
  },
  {
    path: '/sample/list',
    component: () => import('@/app/sample/view/sample-list.page.vue'),
  },
  {
    path: '/sample/add',
    component: () => import('@/app/sample/view/sample-add.page.vue'),
  },
  {
    path: '/sample/detail/:id',
    component: () => import('@/app/sample/view/sample-detail.page.vue'),
  },
];

export default SampleRouter;
