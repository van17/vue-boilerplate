import { Mutation } from 'vuex-class-modules';

import { SampleState } from '@/app/sample/store/sample.state';
import { Sample } from '@/app/sample/model/sample.model';

export class SampleMutation {
  private state!: SampleState;

  constructor(state: SampleState) {
    this.state = state;
  }

  @Mutation
  setList(list: Sample.Response.FindAll[]) {
    this.state.list = JSON.parse(JSON.stringify(list));
  }
 
  @Mutation
  setOne(one: Sample.Response.FindOne) {
    this.state.one = JSON.parse(JSON.stringify(one));
  }
}
