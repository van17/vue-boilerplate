import { VuexModule, Module } from 'vuex-class-modules';

import { store }          from '@/stores';
import { SampleState }    from '@/app/sample/store/sample.state';
import { SampleAction }   from '@/app/sample/store/sample.action';
import { SampleMutation } from '@/app/sample/store/sample.mutation';
import { SampleGetter }   from '@/app/sample/store/sample.getter';

@Module
class SampleStore extends VuexModule {
  private state: SampleState = new SampleState();
  getters: SampleGetter = new SampleGetter(this.state);
  mutations: SampleMutation = new SampleMutation(this.state);
  actions: SampleAction = new SampleAction(this.mutations);
}

export const sampleStore = new SampleStore({ store, name: 'sampleStore' });
