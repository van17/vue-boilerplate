import { Sample } from '@/app/sample/model/sample.model';

export class SampleState {
  list: Sample.Response.FindAll[] = [];
  one: Sample.Response.FindOne = new Sample.Response.FindOne();
}
