import { Action } from 'vuex-class-modules';

import { baseUrl }        from '@/shared/api';
import { SampleMutation } from '@/app/sample/store/sample.mutation';
import { Sample }         from '@/app/sample/model/sample.model';

export class SampleAction {
  private mutations!: SampleMutation;

  constructor(mutations: SampleMutation) {
    this.mutations = mutations;
  }

  @Action
  async getList() {
    const getList = await baseUrl().get(
      '/posts'
    );
    this.mutations.setList(getList.data);
  }

  @Action
  async addItem(params: Sample.Request.Add) {
    await baseUrl().post('/posts', params);
  }

  @Action
  async getOne(id: string) {
    const getOne = await baseUrl().get(
      `/posts/${id}`
    );
    this.mutations.setOne(getOne.data);
  }
}
