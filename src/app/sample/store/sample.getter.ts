import { SampleState } from '@/app/sample/store/sample.state';

export class SampleGetter {
  private state!: SampleState;

  constructor(state: SampleState) {
    this.state = state;
  }

  getterList() {
    return JSON.parse(JSON.stringify(this.state.list));
  }

  getterOne() {
    return JSON.parse(JSON.stringify(this.state.one));
  }
}
