import { validateOrReject } from 'class-validator';

export function validate(obj:any, store) {
  validateOrReject(obj)
    .then(() => {
      return store.actions.addItem(obj);
    })
    .catch((error) => {
      const arr: string[] = [];
      error.forEach(err => {
        arr.push(err);
      });
      const obj = arr.map((ele: any) => {
        return `${ele.property} : ${Object.keys(ele.constraints)} \n`;
      });
      const message = obj.join('');
      alert(message);
    });
}