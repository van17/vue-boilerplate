# Vue-BoilerPlate

- FrontEnd 개발을 위한 기본 개발환경을 제공한다.

## 구성

- 환경
  - Windows 10, mac
  - node `v14.16.1`
  - npm `v6.14.12`
  - Vite `v2.9.1`
  - typescript `v4.6.3`

<!-- blank line -->

- 라이브러리
  - axios `v0.26.1`
    > https://github.com/axios/axios
  - vuex `v3.6.2`
    > https://github.com/vuejs/vuex
  - vuex-class-modules `v1.3.0`
    > https://github.com/gertqin/vuex-class-modules
  - vue-router `v3.5.3`
    > https://github.com/vuejs/vue-router
  - vue-property-decorator `v9.1.2`
    > https://github.com/kaorun343/vue-property-decorator
  - class-transformer `v0.5.1`
    > https://github.com/typestack/class-transformer
  - reflect-metadata `v0.1.13`
    > https://github.com/rbuckton/reflect-metadata
  - vite-plugin-vue2 `v1.9.3`
    > https://github.com/underfin/vite-plugin-vue2
  - vite-plugin-checker `v0.4.6`
    > https://github.com/fi3ework/vite-plugin-checker
  - vue-tsc `v0.34.6`
    > https://www.npmjs.com/package/vue-tsc

<!-- blank line -->

## 설치 및 실행

- 설치
  - [Node 모듈](https://nodejs.org/ko/) 설치
  - [Git](https://git-scm.com/) 설치
  - [WebStorm](https://www.jetbrains.com/ko-kr/webstorm/download/#section=windows) 설치

<!-- blank line -->

- 실행
  - `npm install`
  - `npm run dev`

## 패키지 및 파일 명명 규칙

- 모든 화면 파일명(\*.vue)은 "kebab-case"로 작성한다.
  - Good: sample-list.vue
  - Bad: SampleList.vue
- 모든 화면 파일명(\*.vue)은 기능의 해당하는 두가지 단어 이상으로 작성한다.
  - Good: sample-list.vue
  - Bad: sample.vue
- 모든 기능 파일명(\*.ts)은 "kebab-case" + "dot.case"로 작성한다.
  - Good: platform.service.ts
  - Bad: platform-service.ts
- 모든 폴더는 "kebab-case"로 작성한다.
  - Good: platform, tv-platform
  - Bad: TvPlatform, tvPlatform
- 기타 파일은 "kebab-case"로 작성한다.
  - Good: style.css, default-style.css
  - Bad: defaultStyle.css

## 패키지 구조

```angular2html
 ├── node_modules
 ├── public
 ├── src
      ├── app
           ├── 대분류
               └── 중분류
                   └── 소분류
                       ├── model
                       ├── router
                       ├── store
                       └── view
      ├── assets
      ├── components
      ├── routes
      ├── stores
      ├── validate
      ├── app.vue
      ├── main.ts
 ├── index.html
 ├── package.json
 ├── README.md
 ├── tsconfig.json
 └── vite.config.ts
```

## Component + Vuex + Api

![VuexLifeCycle](https://gitlab.com/van17/vue-boilerplate/-/raw/master/readme_images/component-vuex-api.png)
